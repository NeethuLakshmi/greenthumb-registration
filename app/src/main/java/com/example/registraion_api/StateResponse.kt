package com.example.registraion_api

data class StateResponse (
        val data: List<StateData>,
        val message: String,
        val status: String
)