package com.example.registraion_api


data class CountryResponse(
        val data: List<CountryData>,
        val message: String,
        val status: String
)

data class Error(val status: String?,val message: String?)