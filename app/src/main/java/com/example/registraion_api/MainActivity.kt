package com.example.registraion_api


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private val builder= GsonBuilder()
    private lateinit var cntryId :String
    private lateinit var stateId :String
    private lateinit var cityId:String
    private lateinit var countryList : Array<CountryData>
    private lateinit var cityList : Array<CityData>
    private lateinit var stateList : Array<StateData>
    val gson= builder.serializeNulls().create()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getCountry()
        Contryspinner.onItemSelectedListener = this
        stataeSpinner.onItemSelectedListener = this
        Cityspinner.onItemSelectedListener = this
        registrationbutton.setOnClickListener{
            val firstname=editTextPersonName.toString()
            val lastname=editTextLastName.toString()
            val phone =editTextPhoneNumber.toString()
            val email= editTextEmail.toString()
            val password = editTextPassword.toString()
            val cofirmpassword = editTextConfirmPassword.toString()
            val userType = 1
            registration(firstname,lastname,phone,email,password,cntryId,stateId,cityId,userType)
        }
    }
    private fun registration(firstName: String, lastName: String, phone: String, email: String, password: String, cntryId: String, stateId: String, cityId: String, userType: Int) {
    RetrofitClient.instance.registaionfnn(firstName,lastName,phone,email,password,cntryId,stateId,cityId,userType).enqueue(object :Callback<JsonObject>
    {
        override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
            when {
                response .code() == 400 ->{
                    val regBase = gson.fromJson(response.errorBody()?.charStream(),Error::class.java)
                    Toast.makeText(this@MainActivity,regBase.message,Toast.LENGTH_LONG)
                }
                response.code()== 200 -> {
                    val regBase = gson.fromJson(response.body().toString(),RegistrationResponse::class.java)
                    Toast.makeText(this@MainActivity,regBase.message,Toast.LENGTH_LONG)
                }
                else -> {Toast.makeText(this@MainActivity,"SOMETHING WENT WRONG",Toast.LENGTH_LONG)}
            }
        }

        override fun onFailure(call: Call<JsonObject>, t: Throwable) {
            TODO("Not yet implemented")
        }

    } )
    }
    private fun getCountry() {
        RetrofitClient.instance.countryList().enqueue(object :Callback<JsonObject>{
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                when {
                    response.code()==200 ->{
                        ContryspinnerLoad(response)
                        }

                        response.code() == 400 -> {
                            val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            Toast.makeText(this@MainActivity,res.toString(),Toast.LENGTH_LONG)
                        }
                    }
                }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Toast.makeText(this@MainActivity,"Some error",Toast.LENGTH_LONG)
            }

        })
    }
  private fun ContryspinnerLoad(response: Response<JsonObject>){
      val res = gson.fromJson(response.body().toString(),CountryResponse::class.java)
      countryList = res.data.toTypedArray()
      var country = arrayOfNulls<String>(countryList.size)
      for (i in countryList.indices) {
          country[i] = countryList[i].country_name

      }
      val adapter =  ArrayAdapter(this@MainActivity, android.R.layout.simple_spinner_item,country)
      adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
      Contryspinner.adapter = adapter
  }
    private fun StatespinnerLoad(response: Response<JsonObject>){
        val res = gson.fromJson(response.body().toString(),StateResponse::class.java)
        stateList = res.data.toTypedArray()
        var state = arrayOfNulls<String>(stateList.size)
        for (i in stateList.indices) {
            state[i] = stateList[i].state_name
        }
        val adapter =  ArrayAdapter(this@MainActivity, android.R.layout.simple_spinner_item,state)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        stataeSpinner.adapter = adapter
    }
private fun citySpinnerLoad(response: Response<JsonObject>){
    val res = gson.fromJson(response.body().toString(),CityResponse::class.java)
    cityList = res.data.toTypedArray()
    var city = arrayOfNulls<String>(cityList.size)
    for (i in cityList.indices){
        city[i] = cityList[i].city_name
    }
    val adapter = ArrayAdapter(this,android.R.layout.simple_spinner_item,city)
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    Cityspinner.adapter = adapter

}
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.id) {
            R.id.Contryspinner -> {
                cntryId = countryList.get(position).country_id.toString()
                RetrofitClient.instance.stateList(cntryId).enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                        when {
                            response.code() == 200 -> {

                                StatespinnerLoad(response)
                            }

                            response.code() == 400 -> {
                                val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                                Toast.makeText(this@MainActivity, res.toString(), Toast.LENGTH_LONG)
                            }
                        }
                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                        Toast.makeText(this@MainActivity, "somthing wrong", Toast.LENGTH_LONG)
                    }
                })
            }

            R.id.stataeSpinner -> {
                stateId = stateList.get(position).state_id
                RetrofitClient.instance.cityList(stateId).enqueue(object : Callback<JsonObject> {
                    override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                        when{

                            response.code() == 200 -> {
                                citySpinnerLoad(response)
                            }
                            response.code() == 400 -> {
                                val res = gson.fromJson(response.errorBody()?.charStream(), Error::class.java)
                            }
                    }}

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                        TODO("Not yet implemented")
                    }
                })
            }
           R.id.Cityspinner -> {
               cityId = cityList.get(position).city_id
           }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }



}


