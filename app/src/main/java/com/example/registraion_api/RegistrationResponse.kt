package com.example.registraion_api

data class RegistrationResponse ( val data: RegistrationData,
                                  val message: String,
                                  val status: String)