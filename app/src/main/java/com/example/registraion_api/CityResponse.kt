package com.example.registraion_api

class CityResponse (
        val data: List<CityData>,
        val message: String,
        val status: String
        )
