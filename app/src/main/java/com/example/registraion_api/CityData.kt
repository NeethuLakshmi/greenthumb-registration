package com.example.registraion_api

data class CityData (
            val city_id: String,
            val city_name: String
    )
